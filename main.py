from config import port, baud, cam_address
from dpad import dpad
from analog_left import analog_left

import serial
from inputs import get_gamepad


def send_visca_command(serial_port, command):
    serial_port.write(command)

def main():
    # Open serial port
    with serial.Serial(port, baud, timeout=1) as output_ser:
        while True:
            # Read events from the gamepad
            events = get_gamepad()
            for event in events:
                # dpad
                if event.code == "ABS_HAT0X" or event.code == "ABS_HAT0Y":
                    dpad(event, output_ser)
                # left_analog
                elif event.code == "ABS_X" or event.code == "ABS_Y":
                    analog_left(event, output_ser)

if __name__ == "__main__":
    main()
