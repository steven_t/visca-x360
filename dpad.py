# dpad.py
from config import cam_address

def dpad(event, output_ser):
    if event.code == "ABS_HAT0X" and event.state == -1:
        command = cam_address + b"\x01\x06\x01\x03\x01\x01\x03\xFF"  # Tilt left
        output_ser.write(command)
    elif event.code == "ABS_HAT0X" and event.state == 1:
        command = cam_address + b"\x01\x06\x01\x03\x02\x02\x03\xFF"  # Tilt right
        output_ser.write(command)
    elif event.code == "ABS_HAT0Y" and event.state == -1:
        command = cam_address + b"\x01\x06\x01\x01\x03\x03\x01\xFF"  # Tilt up
        output_ser.write(command)
    elif event.code == "ABS_HAT0Y" and event.state == 1:
        command = cam_address + b"\x01\x06\x01\x02\x03\x03\x02\xFF"  # Tilt down
        output_ser.write(command)
    else:
        command = cam_address + b'\x01\x06\x01\x03\x03\x03\x03\xFF'
        output_ser.write(command)
