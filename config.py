# Serial configuration
baud = 9600
port = "/dev/ttyS0"

# VISCA configuration
cam_address = b"\x81"

# tweaks
deadzone_x = 6000
deadzone_y = 6000

