# analog_left.py
from config import deadzone_x, deadzone_y, cam_address
def convert_analog_input(raw_input,deadzone):
    if raw_input > deadzone:
        scaled_input = int(abs(raw_input/(32767))*20)    # scale input 
        return min(scaled_input, 20).to_bytes(1, "big")  # make sure that range is valid: le \x14

    if raw_input < (-1*deadzone):
        scaled_input = int(abs(raw_input/(32767))*20)  
        return min(scaled_input, 20).to_bytes(1, "big")

    else:
        return min(00, 20).to_bytes(1, 'big')

def direction(raw_input):
    if raw_input > 0:
        return True
    else:
        return False

def analog_left(event, output_ser):
    if event.code == "ABS_X":
        tilt_angle = convert_analog_input(event.state, deadzone_x)
        if direction(event.state):
            command = cam_address + b"\x01\x06\x01" + tilt_angle + tilt_angle + b"\x02\x03\xFF"  # tilt right
            output_ser.write(command)
        else:
            command = cam_address + b"\x01\x06\x01" + tilt_angle + tilt_angle + b"\x01\x03\xFF"  # tilt left
            output_ser.write(command)

    if event.code == "ABS_Y":
        tilt_angle = convert_analog_input(event.state, deadzone_y)
        if direction(event.state):
            command = cam_address + b"\x01\x06\x01" + tilt_angle + tilt_angle + b"\x03\x02\xFF"  # tilt down
            output_ser.write(command)
        else:
            command = cam_address + b"\x01\x06\x01" + tilt_angle + tilt_angle + b"\x03\x01\xFF"  # tilt up
            output_ser.write(command)

